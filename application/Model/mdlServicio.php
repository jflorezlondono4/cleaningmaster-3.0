<?php

namespace Mini\Model;

use Mini\Core\Model;

	class mdlServicio extends Model{

		private $IdServicio;
		private $Nombre;
		private $Costo;
		private $Estado;
		private $Descripcion;

		public function __SET($attr, $value){

			$this->$attr=$value;
		}
		public function __GET($attr){

		 return $this->$attr;
		}

		function __construct(){

			try {
				
				parent::__construct();
			} catch (PDOException $e) {
				exit ("Error en la conexión.");
			}
		}

		public function registrarServicio(){

			$sql="CALL CM_RegistrarServicio(?,?,?,?,?)";
			$stm=$this->db->prepare($sql);
			$stm->bindParam(1, $this->IdServicio);
			$stm->bindParam(2, $this->Nombre);
			$stm->bindParam(3, $this->Costo);
			$stm->bindParam(4, $this->Estado);
			$stm->bindParam(5, $this->Descripcion);
			$stm->execute();

		}

		public function listar(){

			$sql="CALL CM_ListarServicio()";
			$stm=$this->db->prepare($sql);
			$stm->execute();
			return $stm->fetchall();

		}

		public function modificarServicio(){

			$sql="CALL CM_ModificarServicio(?,?,?,?)";
			$stm=$this->db->prepare($sql);
			$stm->bindParam(1, $this->IdServicio);
			$stm->bindParam(2, $this->Nombre);
			$stm->bindParam(3, $this->Costo);
			$stm->bindParam(4, $this->Descripcion);
			$stm->execute();
		}
		
		public function consultarServicio(){

			$sql = "SELECT nombre, costo, estado, descripcion FROM servicio WHERE idservicios = ?";
		$stm = $this->db->prepare($sql);
		$stm ->bindParam(1, $this->IdServicio);
		$stm->execute();
		return $stm->fetch();

		}

		public function cambiarEstado(){
		$sql = "CALL CM_CambiarEstadoServicio(?,?)";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(1, $this->IdServicio);
		$stm->bindParam(2, $this->Estado);
		$stm->execute();
	}

	public function listarServicioActivo(){

			$sql ="CALL CM_ListarServicioActivo()";
			$stm = $this->db->prepare($sql);
			$stm->execute();
			return $stm->fetchall();


		}
		
	}