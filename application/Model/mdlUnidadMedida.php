<?php  
/**
* 
*/
namespace Mini\Model;

use Mini\Core\Model;

class mdlUnidadMedida extends Model
{
	private $idUnidadMedida;
	private $Nombre;
	private $Estado;
	// public $db;

	public function __SET($attr, $value){
		$this->$attr=$value;
	}
	public function __GET($attr){
	return	$this->$attr;
	}
	
	function __construct()
	{
		try {
			parent::__construct();
		} catch (PDOException $e) {
			exit("error en la conexion.");
		}
		
	}

	public function listar(){
		$sql = "CALL CM_ListarUnidadMedida()";
		$stm = $this->db->prepare($sql);
		$stm->execute();
		return $stm->fetchall();
	}

	public function modificar(){
		$sql = "CALL CM_ModificarUnidadMedida(?,?)";
		$stm = $this->db->prepare($sql);
		$stm ->bindParam(1, $this->__GET("idUnidadMedida"));
		$stm ->bindParam(2, $this->__GET("Nombre"));
		$stm->execute();
	}

	public function registrar(){
		
		$sql = "CALL CM_RegistrarUnidadMedida(?,?,?)";
		$stm = $this->db->prepare($sql);
		$stm ->bindParam(1, $this->idUnidadMedida);
		$stm ->bindParam(2, $this->Nombre);		
		$stm ->bindParam(3, $this->Estado);		
		$stm->execute();
	}

	public function cambiarEstado(){
		$sql = "CALL CM_CambiarEstadoUnidadMedida(?,?)";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(1, $this->idUnidadMedida);
		$stm->bindParam(2, $this->Estado);
		$stm->execute();
	}

	public function consultar(){
		$sql = "SELECT nombre, estado FROM unidad_medida WHERE idUnidadMedida = ?";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(1, $this->idUnidadMedida);
		$stm->execute();
		return $stm->fetch();
	}

	

	// public function registrar($codigo,$nombre,$estado){

	// }


}