<?php

namespace Mini\Model;

use Mini\Core\Model;

/**
* 
*/
class mdlOrdenDeServicio extends Model
{
	
	private $Codigo;
	private $Fecha;
	private $Estado;
	private $Empleado;
	private $Marca;
	private $Descripcion;
	private $Placa;
	private $Cliente;
	private $Tvehiculo;
	private $Servicios;



	public function __SET($attr, $value){

		$this->$attr=$value;

	}

	public function __GET($attr){

		return $this->$attr;

	}


	function __construct(){

		try {
			parent::__construct();
		} catch (PDOException $e) {
			exit("Error al conectar con BBDD");
		}

	}

	public function registrarOrdenServicio(){

		$sql= "CALL CM_RegistrarOrdenServicio(?,?,?,?,?,?,?,?)";
		$stm= $this->db->prepare($sql);
		$stm->bindParam(1,$this->Fecha);
		$stm->bindParam(2,$this->Estado);
		$stm->bindParam(3,$this->Empleado);
		$stm->bindParam(4,$this->Marca);
		$stm->bindParam(5,$this->Descripcion);
		$stm->bindParam(6,$this->Placa);
		$stm->bindParam(7,$this->Cliente);
		$stm->bindParam(8,$this->Tvehiculo);
		$stm->execute();

	}

	public function registrarDetalleOrden(){
     	$sql="CALL CM_RegistrarDetalleOrden(?,?)";
     	$stm=$this->db->prepare($sql);
     	$stm->bindParam(1,$this->Codigo);
     	$stm->bindParam(2,$this->Servicios);
     	$stm->execute();
     }

     public function eliminarRegistro(){
          $sql="CALL CM_EliminarDetalleOrden(?)";
          $sql="CALL CM_EliminarOrdenServicio(?)";
          $stm=$this->db->prepare($sql);
          $stm->bindParam(1,$this->Codigo);
          $stm->bindParam(2,$this->Codigo);
          $stm->execute();
     }

     public function consultarDetalle(){
     	$sql = "CALL CM_ConsularDetalleOrden(?)";
     	$stm= $this->db->prepare($sql);
     	$stm->bindParam(1,$this->Codigo);
     	$stm->execute();
     	return $stm->fetchall();
     }
}