<?php 

namespace Mini\Model;

use Mini\Core\Model;

/**
* 
*/
class mdlProducto extends Model
{
	private $idProducto;
	private $Nombre;
	private $PUnitario;
	private $Cantidad;
	private $UMedida;
	private $Estado;
	
	public function __SET($attr,$value){
		$this->$attr=$value;
	}

	public function __GET($attr){
		return $this->$attr;
	}


	function __construct()
	{
		try {
			parent::__construct();
		} catch (PDOException $e) {
			exit("error en la conexion.");
		}
	}

	public function listar(){
		$sql="CALL CM_ListarProducto";
		$stm =$this->db->prepare($sql);
		$stm->execute();
		return $stm->fetchall();
	}

	public function registrarp(){
		$sql="CALL CM_RegistrarProducto(?,?,?,?,?,?)";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(1, $this->idProducto);
		$stm->bindParam(2, $this->Nombre);
		$stm->bindParam(3, $this->PUnitario);
		$stm->bindParam(4, $this->Cantidad);
		$stm->bindParam(5, $this->UMedida);
		$stm->bindParam(6, $this->Estado);
		$stm->execute();

	}

	public function cambiarEstado(){
		$sql = "CALL CM_CambiarEstadoProducto(?,?)";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(1, $this->idProducto);
		$stm->bindParam(2, $this->Estado);
		$stm->execute();
	}
	// CM_ConsultarProducto
	public function consultarProducto(){

		$sql = "CALL CM_ConsultarProducto(?)";
		$stm = $this->db->prepare($sql);
		$stm ->bindParam(1, $this->idProducto);
		$stm ->execute();
		return $stm->fetch();
	}

	public function modificarProducto(){

		$sql="CALL CM_ModificarProducto(?,?,?,?,?)";
		$stm = $this ->db->prepare($sql);
		$stm ->bindParam(1, $this->idProducto);
		$stm ->bindParam(2, $this->Nombre);
		$stm ->bindParam(3, $this->PUnitario);
		$stm ->bindParam(4, $this->Cantidad);
		$stm ->bindParam(5, $this->UMedida);
		$stm ->execute();
	}
	public function modificarCantidad(){
		$sql="CALL CM_ModificarCantidadProducto(?,?)";
		$stm = $this->db->prepare($sql);
		$stm-> bindParam(1,$this->idProducto);
		$stm-> bindParam(2,$this->Cantidad);
		$stm->execute();
	}

}