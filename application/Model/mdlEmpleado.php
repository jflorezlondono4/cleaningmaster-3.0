<?php  
/**
* 
*/
namespace Mini\Model;

use Mini\Core\Model;

class mdlEmpleado extends Model
{
	private $tipoDocumento;
	private $documento;
	private $nombre;
	private $Tipo_Persona_idTipo_Persona;
	private $contacto;
	private $direccion;
	private $estado;
	// public $db;

	public function __SET($attr, $value){
		$this->$attr=$value;
	}
	public function __GET($attr){
	return	$this->$attr;
	}
	
	function __construct()
	{
		try {
			parent::__construct();
		} catch (PDOException $e) {
			exit("error en la conexion.");
		}
		
	}

	public function listarEmpleado(){
		$sql = "CALL CM_ListarEmpleado()";
		$stm = $this->db->prepare($sql);
		$stm->execute();
		return $stm->fetchall();
	}
      public function listarEmpleadoActivo(){
		$sql = "CALL CM_ListarEmpleadoActivo()";
		$stm = $this->db->prepare($sql);
		$stm->execute();
		return $stm->fetchall();
	}
	public function listarTipoPersona(){
		$sql = "CALL CM_ListarTipoPersona()";
		$stm = $this->db->prepare($sql);
		$stm->execute();
		return $stm->fetchall();
	}

	public function modificarEmpleado(){
		$sql = "CALL CM_ModificarEmpleado(?,?,?,?,?,?,?)";
		$stm = $this->db->prepare($sql);
		$stm ->bindParam(1, $this->tipoDocumento);
		$stm ->bindParam(2, $this->documento);
		$stm ->bindParam(3, $this->nombre);
		$stm ->bindParam(4, $this->Tipo_Persona_idTipo_Persona);
		$stm ->bindParam(5, $this->contacto);
		$stm ->bindParam(6, $this->direccion);
		$stm ->bindParam(7, $this->estado);
		$stm->execute();
	}

	public function registrarEmpleado(){
		
		$sql = "CALL CM_RegistrarEmpleado(?,?,?,?,?,?,?)";
		$stm = $this->db->prepare($sql);
		$stm ->bindParam(1, $this->tipoDocumento);
		$stm ->bindParam(2, $this->documento);
		$stm ->bindParam(3, $this->nombre);
		$stm ->bindParam(4, $this->Tipo_Persona_idTipo_Persona);
		$stm ->bindParam(5, $this->contacto);
		$stm ->bindParam(6, $this->direccion);
		$stm ->bindParam(7, $this->estado);
		$stm->execute();
	}

	public function cambiarEstadoEmpleado(){
		$sql = "CALL CM_CambiarEstadoEmpleado(?,?)";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(1, $this->documento);
		$stm->bindParam(2, $this->Estado);
		$stm->execute();
	}

	public function consultarEmpleado(){
		$sql = "SELECT documento,tipoDocumento,nombre,contacto,direccion,estado FROM persona WHERE documento = ?";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(1, $this->documento);
		$stm->execute();
		return $stm->fetch();
	}

	

	// public function registrar($codigo,$nombre,$estado){

	// }


}