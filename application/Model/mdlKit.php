<?php  
/**
* 
*/
namespace Mini\Model;

use Mini\Core\Model;

class mdlKit extends Model
{
	private $Codigo;
	private $Nombre;
	private $TVehiculo;
	private $Servicio;
	private $Estado;
	private $Cantidad;
	private $Producto;

	public function __SET($attr,$value){
		$this-> $attr=$value;
	}
	public function __GET($attr){
		return $this->$attr;
	}
	function __construct()
	{
		try {
			parent::__construct();
		} catch (PDOException $e) {
			exit("error en la conexion.");
		}
	}

     public function registrarKit(){
     	$sql="CALL CM_RegistrarKit(?,?,?,?,?)";
     	$stm= $this->db->prepare($sql);
     	$stm->bindParam(1,$this->Codigo);
     	$stm->bindParam(2,$this->Nombre);
     	$stm->bindParam(3,$this->TVehiculo);
     	$stm->bindParam(4,$this->Servicio);
     	$stm->bindParam(5,$this->Estado);
     	$stm->execute();
     }
     public function listarKit(){
     	$sql="CALL CM_ListarKit()";
     	$stm=$this->db->prepare($sql);
     	$stm->execute();
       return $stm->fetchall();

     }
     public function registrarDetalle(){
     	$sql="CALL CM_RegistrarDetalle(?,?,?)";
     	$stm=$this->db->prepare($sql);
     	$stm->bindParam(1,$this->Cantidad);
     	$stm->bindParam(2,$this->Codigo);
     	$stm->bindParam(3,$this->Producto);
     	$stm->execute();
     }
     public function consultarDetalle(){
     	$sql = "CALL CM_ConsularDetalles(?)";
     	$stm= $this->db->prepare($sql);
     	$stm->bindParam(1,$this->Codigo);
     	$stm->execute();
     	return $stm->fetchall();
     }
     public function eliminarRegistro(){
          $sql="CALL CM_EliminarDetalle(?)";
          $sql="CALL CM_EliminarKit(?)";
          $stm=$this->db->prepare($sql);
          $stm->bindParam(1,$this->Codigo);
          $stm->bindParam(2,$this->Codigo);
          $stm->execute();
     }

}