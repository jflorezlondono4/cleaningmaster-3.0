<?php 

namespace Mini\Model;

use Mini\Core\Model;

/**
* 
*/
class mdlMovimiento extends Model
{
	private $Codigo;
	private $Cantidad;
	private $Producto;
	private $Descripcion;
	private $Tipo;
	

	public function __SET($attr,$value){
		$this-> $attr=$value;
	}
	public function __GET($attr){
		return $this->$attr;
	}

	function __construct()
	{
		try {
			parent::__construct();
		} catch (PDOException $e) {
			exit("error en la conexion.");
		}
	}

	public function listarMovimiento(){
		$sql = "CALL CM_ListarMovimiento(?)";
		$stm = $this->db->prepare($sql);
		$stm ->bindParam(1, $this->Tipo);
		$stm ->execute();
		return $stm->fetchall();
	}

	public function registrarMovimiento(){
		$sql = "CALL CM_RegistrarMovimiento(?,?,?,?,?)";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(1, $this->Codigo);
		$stm->bindParam(2, $this->Cantidad);
		$stm->bindParam(3, $this->Producto);
		$stm->bindParam(4, $this->Descripcion);
		$stm->bindParam(5, $this->Tipo);
		$stm->execute();
	}


}