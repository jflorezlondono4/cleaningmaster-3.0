<?php  

namespace Mini\Model;

use Mini\Core\Model;

	class mdlTipoVehiculo extends Model
	{
		
		private $IdTipoVehiculo;
		private $Nombre;
		private $Estado;
		private $Descripcion;


			public function __SET($attr, $value){
			$this->$attr=$value;
			 }
			public function __GET($attr){
			return	$this->$attr;
				}
		function __construct(){
		try {
			parent::__construct();
		} catch (PDOException $e) {
			exit("error en la conexion.");
		}
		
	}

		public function listar(){
		$sql = "CALL CM_ListarTipoVehiculo()";
		$stm = $this->db->prepare($sql);
		$stm->execute();
		return $stm->fetchall();
	}

		
	public function registrarTVehiculo(){
		
		$sql = "CALL CM_RegistrarTipoVehiculo(?,?,?,?)";
		$stm = $this->db->prepare($sql);
		$stm ->bindParam(1, $this->IdTipoVehiculo);
		$stm ->bindParam(2, $this->Nombre);		
		$stm ->bindParam(3, $this->Estado);
		$stm ->bindParam(4, $this->Descripcion);		
		$stm->execute();

	}
		public function modificarTVehiculo(){
		$sql = "CALL CM_ModificarTipoVehiculo(?,?,?)";
		$stm = $this->db->prepare($sql);
		$stm ->bindParam(1, $this->IdTipoVehiculo);
		$stm ->bindParam(2, $this->Nombre);
		$stm ->bindParam(3, $this->Descripcion);
		$stm->execute();
	}

	public function consultarTVehiculo(){
		$sql = "SELECT nombre, estado, observaciones FROM tipo_vehiculo WHERE idtipo_vehiculo = ?";
		$stm = $this->db->prepare($sql);
		$stm ->bindParam(1, $this->IdTipoVehiculo);
		$stm->execute();
		return $stm->fetch();
	}

	public function cambiarEstado(){
		$sql = "CALL CM_CambiarEstadoTipoVehiculo(?,?)";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(1, $this->IdTipoVehiculo);
		$stm->bindParam(2, $this->Estado);
		$stm->execute();
	}
	public function listarTipoVehiculoActivo(){

		$sql ="CALL CM_ListarTipoVehiculoActivo()";
		$stm = $this->db->prepare($sql);
		$stm->execute();
		return $stm->fetchall();
	}

	}
