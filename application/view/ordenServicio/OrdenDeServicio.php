        <form method="POST" action="<?php echo URL;?> OrdenServicio/botones" class="form-horizontal">
          <div class="row">
            <div class="col-md-12">
              <div class="widget">
                <div class="widget-heading">
                  <h3 class="widget-title">ORDEN DE SERVICIO</h3>
                </div>
                <div class="widget-body">
                  
                    <div class="form-group">
                      <label for="textInput">Cliente</label>
                      <select  type="text" class="form-control" name="cliente" id="cliente">
                       <option value=""></option>
                       <?php foreach ($cliente as $value): ?>
                        <option value="<?= $value['documento'];?>"><?= $value['nombre'];?></option>
                      <?php endforeach?>
                    </select>
                    </div>
                    <div class="form-group">
                      <label for="textInput">Placa</label>
                      <input  type="text" required class="form-control" name="placa" id="placa">
                    </div>
                     <div class="form-group">
                      <label for="textInput">Marca</label>
                      <input  type="text" required class="form-control" name="marca" id="marca">
                    </div>
                     <div class="form-group">
                      <label for="textInput">Fecha</label>
                      <input  type="text" value="<?= date("y-m-d"." "."h:i:s"); ?>" required class="form-control" name="fecha" id="fecha">
                    </div>
                     <div class="form-group">
                      <label for="textInput">Tipo Vehiculo</label>
                      <select  type="text" class="form-control" name="tvehiculo" id="tvehiculo">
                       <option value=""></option>
                       <?php foreach ($Vehiculo as $value): ?>
                        <option value="<?= $value['idtipo_vehiculo'];?>"><?= $value['nombre'];?></option>
                      <?php endforeach?>
                    </select>
                    </div>
                    <div class="form-group">
                      <label for="textInput">Empleado</label>
                      <select  type="text" class="form-control" name="empleado" id="empleado">
                       <option value=""></option>
                        <?php foreach ($empleado as $value): ?>

                        <option value="<?= $value['documento'];?>"><?= $value['nombre'];?></option>
                      <?php endforeach  ?>
                    </select>
                    </div>
                    <div class="form-group">
                      <label for="textInput">Estado</label>
                      <select  type="text" class="form-control" name="estado" id="estado">
                       <option value=""></option>
                       <option value="1">En proceso</option>
                        <option value="2">pagada</option>
                    </select>
                    </div>
                    <div class="form-group">
                      <label for="textArea">Descripciòn</label>
                      <textarea  rows="3" class="form-control" name="descripcion" id="descripcion"></textarea>
                    </div>
                  
                  
                </div>
              </div>
            </div>
          </div>
           <input class="btn btn-primary pull-right" type="button" id="agregar_servicio" value="Agregar Servicios" />

                    <br>
                    <br>
                    <br>
                     <legend class="section"> SERVICIOS </legend>

                     <div class="form-group">
                    <label for="textInput">Servicio</label>
                       <select class="form-control" disabled="disabled" id="servicios" name="servicios">
                      <option value="0"></option>
                      <?php foreach ($servicios as $value): ?>
                      
                        <option  value="<?=$value["idservicios"]?>"><?=$value["nombre"]?></option>
                      
                      <?php endforeach  ?>
                    </select>
                    </div>
                    <input class="btn btn-primary pull-right" disabled="disabled" type="button" id="agregar_detalleOrden" value="+" />
         
                    <div class="col-lg-8">
          <div class="widget">
           
            <div class="widget-content">
              <table id="mytabla" class="table table-hover">
                <thead>
                  <tr>
                    <th>Servicio</th>
                  </tr>
                </thead>
                <tbody>
    
                </tbody>
              </table>
            </div>
          </div>
        </div>
                      <br>
                      <br>
                      <br>
                      <br>
                      <br>
                 <div class="form-actions">
                  
                  <center>

                    <input class="btn btn-primary" type="submit" name="submit_guardar" value="FINALIZAR REGISTRO" />
                    <input class="btn btn-danger" type="submit" name="submit_cancelar" value="CANCELAR" />
                   
                    </center>
                 
                </div>

         <!--  <div class="col-lg-6" style="margin-top: -21px;">
                    <h2 style="margin-left: 70px;">SERVICIOS</h2>
                    <br>
                    <?php /* foreach ($servicios as $value): ?>
                      <div class="form-group lable-padd">
                 <label class="col-md-3"><?= $value['nombre'];?></label>
                   <div class="col-md-6">
                    <input type="hidden" name="idServicio" value="<?= $value['idservicios'];?>" >
                     <select class="form-control required" name="dd1">
                      <option value=""></option>
                      <option value="1">SI</option>
                      <option value="2">NO</option>
                    </select>
                   </div>
                 </div>
                    <?php endforeach */ ?>
                </div> -->




          <center>
             <div class="panel-footer">
                <div class="row">
                  <div class="col-sm-6 col-sm-offset-3">
                    <div class="btn-toolbar">
                      <button class="btn-primary btn" onclick="javascript:$('#validate-form').parsley( 'validate' );">IMPRIMIR</button>
                      <button class="btn-info btn">LISTAR</button>
                    </div>
                  </div>
                </div>
              </div>
              </center>
            </from>