       <div class="row">
            <div class="col-md-12">
              <div class="widget">
                <div class="widget-heading">
                  <h3 class="widget-title">EMPLEADO</h3>
                </div>
                <div class="widget-body">
                  <form method="POST" action="<?php echo URL; ?>Empleado/guardarEmpleado" class="form-horizontal">
                   
                    <div class="form-group">
                      <label for="textInput">Tipo de Documento</label>
                      <select class="form-control required" name="dd1">
                      <option value=""></option>
                      <option value="1">Tarjeta de Identidad</option>
                      <option value="2">Cedula de Ciudadania</option>
                      <option value="3">Cedula extranjera</option>
                    </select>
                    </div>
                     <div class="form-group">
                      <label for="textInput">Documento</label>
                      <input  type="number" required class="form-control" name="cedula">
                    </div>
                    <div class="form-group">
                      <label for="textInput">Nombre</label>
                      <input  type="text" required class="form-control" name="Nombres">
                    </div>
                     <div class="form-group">
                    <label for="textInput">Tipo de Persona</label>
                       <select class="form-control required" name="dd2">
                      <option value=""></option>
                       <?php foreach ($persona as $value): ?>
                        <option value="<?= $value['idtipoPersona'];?>"><?= $value['nombre'];?></option>
                      <?php endforeach  ?>
                    </select>
                    </div>
                     <div class="form-group">
                      <label for="textInput">Numero de Contacto</label>
                      <input type="number" class="form-control mask" data-inputmask="'mask':'+57 999 999 999'" name="Contactos" />
                    </div>
                     <div class="form-group">
                      <label for="textInput">Direcciòn</label>
                      <input type="text"  class="form-control" name="Direcciones">
                    </div>
                    <div class="form-group">
                    <label for="textInput">Estado</label>
                       <select class="form-control required" name="dd2">
                      <option value=""></option>
                      <option value="1">ACTIVO</option>
                      <option value="2">INACTIVO</option>
                    </select>
                    </div>
                    
                  <div class="form-actions">
                  <div>
                  <center>
                    <button class="btn btn-primary" type="submit_guardar">GUARDAR</button>
                    
                    </center>
                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
          
           <div class="col-md-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-hand-up"></i>
              <h3>EMPLEADOS</h3>
            </div>
            <div class="widget-content">
              <table class="table">
                <thead>
                  <tr>
                    <th>TIPO DOCUMENTO</th>
                    <th>DOCUMENTO</th>
                    <th>NOMBRE</th>
                    <th>TIPO EMPLEADO</th>
                    <th>CONTACTO</th>
                    <th>DIRECCION</th>
                    <th>ESTADO</th>
                    <th>OPCIONES</th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($datos as $value): ?>
                  <tr>
                    <td><?=$value["tipoDocumento"]?></td>
                    <td><?=$value["documento"]?></td>
                    <td><?=$value["nombre"]?></td>
                    <td><?=$value["Tipo_Persona_idTipo_Persona"]?></td>
                    <td><?=$value["contacto"]?></td>
                    <td><?=$value["direccion"]?></td>
                    <td><?=$value["estado"]==1?"Activo":"Inactivo"?></td>
                    <td>
                      <a href="<?php echo URL; ?>empleado/edit/<?=$value["documento"] ?>">editar</a>

                      <?php if ($value["estado"]==1){ ?>
                      <a href="#" onclick="cambiarEstadoEmpleado(<?=$value["documento"] ?>,0)">Inhabilitar</a>
                      <?php }
                      else{ ?>
                      <a href="#" onclick="cambiarEstadoEmpleado(<?=$value["documento"] ?>,1)">Activar</a>
                      <?php } ?>
                    </td>
                  </tr>

                <?php endforeach; ?>
  
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>