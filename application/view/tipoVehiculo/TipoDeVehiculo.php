  
  <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
              <div class="widget">
                <div class="widget-heading">
                  <h3 class="widget-title">TIPO DE VEHICULO</h3>
                </div>
                <div class="widget-body">
                  <form method="post" class="form-horizontal" action="<?php echo URL; ?>TipoVehiculo/registrar">
                   
                    <div class="form-group">
                      <label for="textInput">Codigo</label>
                      <input  type="number" required class="form-control" name="codigo" >
                    </div>
                     <div class="form-group">
                      <label for="textInput">Nombre</label>
                      <input  type="text" required class="form-control" name="nombre">
                    </div>
                    <div class="form-group">
                    <label for="textInput">Estado</label>
                       <select class="form-control required" name="estado">
                      <option value="0"></option>
                      <option value="1">ACTIVO</option>
                      <option value="0">INACTIVO</option>
                    </select>
                    </div>
                     <div class="form-group">
                      <label for="textArea">Descripciòn</label>
                      <textarea id="textArea" rows="3" class="form-control" name="description"></textarea>
                    </div>
                 <div class="form-actions">
                  <div>
                  <center>
                    <input class="btn btn-primary" type="submit" name="submit_guardar" value="REGISTRAR" />
                   
                    </center>
                  </div>
                </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <BR><BR>
<div class="row">
  <div class="col-md-1"></div>
  <div class="col-lg-10">
   <div class="widget-header"> <i class="icon-hand-up"></i>
              <h3>TIPO VEHICULO</h3>
            </div>
  <div class="widget-content">
              <table class="table">
                <thead>
                  <tr>
                    <th>CÓDIGO</th>
                    <th>NOMBRE</th>
                    <th>DESCRIPCIÓN</th>
                    <th>ESTADO</th>
                    <th>OPCIONES</th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($datos as $value): ?>
                  <tr>
                    <td><?=$value["idtipo_vehiculo"]?></td>
                    <td><?=$value["nombre"]?></td>
                    <td><?=$value["observaciones"]?></td>
                    <td><?=$value["estado"]==1?"Activo":"Inactivo"?></td>
                    <td>
                      <a href="<?php echo URL; ?>TipoVehiculo/edit/<?=$value["idtipo_vehiculo"] ?>">Editar</a>

                      <?php if ($value["estado"]==1){ ?>
                      <a href="#" onclick="cambiarEstadoVehiculo(<?=$value["idtipo_vehiculo"]?>,0)">Inhabilitar</a>
                      <?php }else{ ?>
                      <a href="#" onclick="cambiarEstadoVehiculo(<?=$value["idtipo_vehiculo"] ?>,1)">Activar</a>
                      <?php } ?>
                    </td>
                  </tr>

                <?php endforeach; ?>
  
                </tbody>
              </table>
            </div>
     </div>
  </div>
         