 <div class="row">
            <div class="col-md-6" >
              <div class="widget">
                <div class="widget-heading">
                  <h3 class="widget-title">ENTRADA</h3>
                </div>
                <div class="widget-body">
                  <form method="post" action="<?php echo URL; ?>entradaYsalida/registrarEntrada"  class="form-horizontal">
                   
                    <div class="form-group">
                      <label for="textInput">Codigo</label>
                      <input  type="number" required class="form-control" name="codigo" >
                    </div>
                     <div class="form-group">
                      <label for="textInput">Cantidad</label>
                      <input  type="number" required class="form-control" name="cantidad">
                    </div>
                    <div class="form-group">
                    <label for="textInput">Producto</label>
                       <select class="form-control required" name="producto">
                      <option value=""></option>
                      <?php foreach ($productos as $value):  ?>
                        <option value="<?=$value["idproducto"];?>"><?=$value["nombre"];?></option>
                      <?php endforeach ?> 
                      <!-- <option value="1">ESTOPA</option>
                      <option value="2">ACPM</option>
                      <option value="3">SHAMPOO</option> -->
                    </select>
                    </div>
                     <div class="form-group">
                      <label for="textArea">Descripciòn</label>
                      <textarea id="textArea" rows="3" class="form-control" name="description"></textarea>
                    </div>
                    
                <div class="form-actions">
                  <div>
                  <center>
                    <input class="btn btn-primary" type="submit" value="REGISTRAR" />
                    </center>
                  </div>
                </div>
                  </form>
                </div>
              </div>
            </div>
            <div class="col-md-6" >
              <div class="widget">
                <div class="widget-heading">
                  <h3 class="widget-title">SALIDA</h3>
                </div>
                <div class="widget-body">
                  <form method="post" action="<?php echo URL; ?>entradaYsalida/registrarSalida"  class="form-horizontal">
                   
                    <div class="form-group">
                      <label for="textInput">Codigo</label>
                      <input  type="number" required class="form-control" name="codigo" >
                    </div>
                     <div class="form-group">
                      <label for="textInput">Cantidad</label>
                      <input  type="number" required class="form-control" name="cantidad">
                    </div>
                    <div class="form-group">
                    <label for="textInput">Producto</label>
                       <select class="form-control required" name="producto">
                      <option value=""></option>
                      <?php foreach ($productos as $value):  ?>
                        <option value="<?=$value["idproducto"];?>"><?=$value["nombre"];?></option>
                      <?php endforeach ?> 
                      <!-- <option value="1">ESTOPA</option>
                      <option value="2">ACPM</option>
                      <option value="3">SHAMPOO</option> -->
                    </select>
                    </div>
                      <div class="form-group">
                      <label for="textArea">Descripciòn</label>
                      <textarea id="textArea" rows="3" class="form-control" name="description"></textarea>
                    </div>
                <div class="form-actions">
                  <div>
                  <center>
                    <input class="btn btn-primary" type="submit" value="REGISTRAR" />
                    </center>
                  </div>
                </div>
                  </form>
                </div>
              </div>
            </div>
            </div>
            <div class="row">
    <div class="col-lg-6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-hand-up"></i>
              <h3>ENTRADAS</h3>
            </div>
            <div class="widget-content">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>CODIGO</th>
                    <th>CANTIDAD</th>
                    <th>PRODUCTO</th>
                    <th>DESCRIPCION</th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($Entrada as $value): ?>
                  <tr>
                    <td><?=$value["idmovimiento"]?></td>
                    <td><?=$value["cantidad"]?></td>
                    <td><?=$value["nombre"]?></td>
                    <td><?=$value["descripcion"]?></td>
                  </tr>
                <?php endforeach; ?>
  
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-hand-up"></i>
              <h3>SALIDAS</h3>
            </div>
            <div class="widget-content">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>CODIGO</th>
                    <th>CANTIDAD</th>
                    <th>PRODUCTO</th>
                    <th>DESCRIPCION</th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($Salida as $value): ?>
                  <tr>
                    <td><?=$value["idmovimiento"]?></td>
                    <td><?=$value["cantidad"]?></td>
                    <td><?=$value["nombre"]?></td>
                    <td><?=$value["descripcion"]?></td>
                  </tr>
                <?php endforeach; ?>
  
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </div>