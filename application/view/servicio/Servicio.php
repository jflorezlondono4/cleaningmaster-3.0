  
  <div class="row">
    <div class="col-md-1"></div>
            <div class="col-md-10" >
              <div class="widget">
                <div class="widget-heading">
                  <h3 class="widget-title">SERVICIO</h3>
                </div>
                <div class="widget-body">
                  <form  method="post" class="form-horizontal" action="<?php echo URL;?>Servicio/registrar">
                   
                    <div class="form-group">
                      <label for="textInput">Codigo</label>
                      <input  type="number" required class="form-control" name="codigo" >
                    </div>
                     <div class="form-group">
                      <label for="textInput">Nombre</label>
                      <input  type="text" required class="form-control" name="nombre">
                    </div>
                    <div class="form-group">
                      <label for="textInput">Costo</label>
                      <input  type="text" required class="form-control" name="costo">
                    </div>
                    <div class="form-group">
                    <label for="textInput">Estado</label>
                       <select class="form-control required" name="estado">
                      <option value="0"></option>
                      <option value="1">ACTIVO</option>
                      <option value="0">INACTIVO</option>
                    </select>
                    </div>
                     <div class="form-group">
                      <label for="textArea">Descripciòn</label>
                      <textarea id="textArea" rows="3" class="form-control"></textarea>
                    </div>
                 <div class="form-actions">
                  <div>
                  <center>
                    <input class="btn btn-primary" type="submit" name="submit_guardar" value="REGISTRAR" />
                   
                    </center>
                  </div>
                </div>
                  </form>
                </div>
              </div>
            </div>

        
          </div>
          <BR><BR><BR><BR><BR>
           <div class="row">
            <div class="col-md-1"></div>
  <div class="col-lg-10">
  <div class="widget-header"> <i class="icon-hand-up"></i>
              <h3>SERVICIO</h3>
            </div>
  <div class="widget-content">
              <table class="table">
                <thead>
                  <tr>
                    <th>CÓDIGO</th>
                    <th>NOMBRE</th>
                    <th>COSTO</th>
                    <th>DESCRIPCIÓN</th>
                    <th>ESTADO</th>
                    <th>OPCIONES</th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($datos as $value): ?>
                  <tr>
                    <td><?=$value["idservicios"]?></td>
                    <td><?=$value["nombre"]?></td>
                    <td><?=$value["costo"]?></td>
                    <td><?=$value["descripcion"]?></td>
                    <td><?=$value["estado"]==1?"Activo":"Inactivo"?></td>
                    <td>
                      <a href="<?php echo URL; ?>Servicio/edit/<?=$value["idservicios"] ?>">Editar</a>

                      <?php if ($value["estado"]==1){ ?>
                      <a href="#" onclick="cambiarEstadoServicio(<?=$value["idservicios"]?>,0)">Inhabilitar</a>
                      <?php }else{ ?>
                      <a href="#" onclick="cambiarEstadoServicio(<?=$value["idservicios"]?>,1)">Activar</a>
                      <?php } ?>
                    </td>
                  </tr>

                <?php endforeach; ?>
  
                </tbody>
              </table>
            </div>
     </div>
  </div>