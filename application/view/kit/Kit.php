  
  <div class="row">
    <div class="col-md-1"></div>
            <div class="col-md-10" >
              <div class="widget">
                <div class="widget-heading">
                  <h3 class="widget-title">KIT</h3>
                </div>
                <div class="widget-body">
                  <form  method="POST" class="form-horizontal" action="<?php echo URL;?>Kit/botones">
                   
                    <div class="form-group">
                      <label for="textInput">Codigo</label>
                      <input  type="number" required class="form-control" id="codigo" name="codigo" >
                    </div>
                     <div class="form-group">
                      <label for="textInput">Nombre</label>
                      <input  type="text" required class="form-control" id="nombre" name="nombre">
                    </div>
                    <div class="form-group">
                    <label for="textInput">Tipo de vehiculo</label>
                       <select class="form-control required" id="vehiculo" name="vehiculo">
                      <option value="0"></option>
                      <?php foreach ($Vehiculo as $value): ?>
                      <?php if ($value["estado"]==1): ?>
                        <option value="<?=$value["idtipo_vehiculo"]?>"><?=$value["nombre"]?></option>
                      <?php endif ?>
                      <?php endforeach ?>
                    </select>
                    </div>
                    <div class="form-group">
                    <label for="textInput">Servicio</label>
                       <select class="form-control required" id="servicio" name="servicio">
                      <option value="0"></option>
                      <?php foreach ($Servicio as $value): ?>
                      <?php if ($value["estado"]==1): ?>
                        <option value="<?=$value["idservicios"]?>"><?=$value["nombre"]?></option>
                      <?php endif ?>
                      <?php endforeach  ?>
                    </select>
                    </div>
                    <div class="form-group">
                    <label for="textInput">Estado</label>
                       <select class="form-control" id="estado" name="estado">
                      <option value="0"></option>
                      <option value="1">ACTIVO</option>
                      <option value="0">INACTIVO</option>
                    </select>
                    </div>
                    <input class="btn btn-primary pull-right" type="button" id="agregar_producto" value="Agregar Productos" />

                    <br>
                    <br>
                    <br>
                     <legend class="section"> PRODUCTOS </legend>
                     <div class="form-group">
                      <label for="textInput">Cantidad</label>
                      <input  type="number" disabled="disabled" class="form-control" id="cantidad" name="cantidad">
                    </div>
                    <div class="form-group">
                    <label for="textInput">Producto</label>
                       <select class="form-control" disabled="disabled" id="producto" name="producto">
                      <option value="0"></option>
                      <?php foreach ($Producto as $value): ?>
                      <?php if ($value["estado"]==1): ?>
                        <option  value="<?=$value["idproducto"]?>"><?=$value["nombre"]?></option>
                      <?php endif ?>
                      <?php endforeach  ?>
                    </select>
                    </div>
                    <input class="btn btn-primary pull-right" disabled="disabled" type="button" id="agregar_detalle" value="+" />
                      <!-- btn-outline -->

                      <div class="col-lg-8">
          <div class="widget">
           
            <div class="widget-content">
              <table id="mytabla" class="table table-hover">
                <thead>
                  <tr>
                    <th>Producto</th>
                    <th>Cantidad</th>
                    
                  </tr>
                </thead>
                <tbody>
    
                </tbody>
              </table>
            </div>
          </div>
        </div>

                      <br>
                      <br>
                      <br>
                      <br>
                      <br>
                 <div class="form-actions">
                  
                  <center>

                    <input class="btn btn-primary" type="submit" name="submit_guardar" value="FINALIZAR REGISTRO" />
                    <input class="btn btn-danger" type="submit" name="submit_cancelar" value="CANCELAR" />
                   
                    </center>
                 
                </div>
                  </form>
                </div>
              </div>
            </div>

        
          </div>
          <BR><BR>
         <div class="row">
            <div class="col-md-1"></div>
  <div class="col-lg-10">
  <div class="widget-header"> <i class="icon-hand-up"></i>
              <h3>KIT</h3>
            </div>
  <div class="widget-content">
              <table class="table">
                <thead>
                  <tr>
                    <th>CÓDIGO</th>
                    <th>NOMBRE</th>
                    <th>TIPO VEHICULO</th>
                    <th>SERVICIO</th>
                    <th>ESTADO</th>
                    <th>OPCIONES</th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($Kitt as $value): ?>
                  <tr>
                    <td><?=$value["idkit"]?></td>
                    <td><?=$value["nombre"]?></td>
                    <td><?=$value["SNombre"]?></td>
                    <td><?=$value["TNombre"]?></td>
                    <td><?=$value["estado"]==1?"Activo":"Inactivo"?></td>
                    <td>
                      <a href="<?php echo URL; ?>Servicio/edit/<?=$value["idkit"] ?>">Editar</a>

                      <?php if ($value["estado"]==1){ ?>
                      <a href="#" onclick="cambiarEstadoServicio(<?=$value["idkit"]?>,0)">Inhabilitar</a>
                      <?php }else{ ?>
                      <a href="#" onclick="cambiarEstadoServicio(<?=$value["idkit"]?>,1)">Activar</a>
                      <?php } ?>
                    </td>
                  </tr>

                <?php endforeach; ?>
  
                </tbody>
              </table>
            </div>
     </div>
  </div> 