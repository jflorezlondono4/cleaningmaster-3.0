 <div class="row">
  <div class="col-md-1"></div>
            <div class="col-md-10">
              <div class="widget">
                <div class="widget-heading">
                  <h3 class="widget-title">PRODUCTO</h3>
                </div>
                <div class="widget-body">
                  <form method="post" action="<?php echo URL; ?>Producto/registrar" class="form-horizontal">
                   
                    <div class="form-group">
                      <label for="textInput">Codigo</label>
                      <input  type="number" required class="form-control" name="codigo" >
                    </div>
                     <div class="form-group">
                      <label for="textInput">Nombre</label>
                      <input  type="text" required class="form-control" name="nombre">
                    </div>
                    <div class="form-group">
                      <label for="textInput">Precio Unitario</label>
                      <input  type="text" required class="form-control" name="precio">
                    </div>
                    <div class="form-group">
                      <label for="textInput">Cantidad</label>
                      <input  type="text" required class="form-control" name="cantidad">
                    </div>
                      <div class="form-group">
                    <label for="textInput">Unidad de Medida</label>
                      <select class="form-control required" name="medida">
                      <option value=""></option>
                      <?php foreach ($Medida as $value):?>
                        <?php if ($value['estado']==1): ?>
                      <option value="<?= $value['idUnidadMedida'] ?>"><?= $value['nombre'] ?></option>
                       <?php endif ?>
                    <?php endforeach ?>
                    <!-- <option value="1">DOCENA</option>
                      <option value="2">CAJA DE 4</option>
                      <option value="2">CAJA DE 6</option> -->
                    </select>
                    </div>
                    <div class="form-group">
                    <label for="textInput">Estado</label>
                       <select class="form-control required" name="estado">
                      <option value="0"></option>
                      <option value="1">ACTIVO</option>
                      <option value="0">INACTIVO</option>
                    </select>
                    </div>
                    
                <div class="form-actions">
                  <div>
                  <center>
                    <input class="btn btn-primary" name="submit_guardar" type="submit" value="REGISTRAR" />
                   <!--  <button class="btn btn-info" type="button">CONSULTAR</button>
                    <button class="btn btn-primary" type="button">MODIFICAR</button>
                    <button class="btn btn-info" type="button">LISTAR</button> -->
                    </center>
                  </div>
                </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-1"></div>
             <div class="col-lg-10">
          <div class="widget">
            <div class="widget-header"> <i class="icon-hand-up"></i>
              <h3>PRODUCTOS</h3>
            </div>
            <div class="widget-content">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>CODIGO</th>
                    <th>NOMBRE</th>
                    <th>PRECIO UNITARIO</th>
                    <th>CANTIDAD</th>
                    <th>UNIDAD MEDIDA</th>
                    <th>ESTADO</th>
                    <th>OPCIONES</th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($Datos as $value): ?>
                  <tr>
                    <td><?=$value["idproducto"]?></td>
                    <td><?=$value["nombre"]?></td>
                    <td><?=$value["precio_unitario"]?></td>
                    <td><?=$value["cantidad"]?></td>
                    <td><?=$value["medida"]?></td>
                    <td><?=$value["estado"]==1?"Activo":"Inactivo"?></td>
                    <td>
                      <a href="<?php echo URL; ?>Producto/edit/<?=$value["idproducto"] ?>">editar</a>

                      <?php if ($value["estado"]==1){ ?>
                      <a href="#" onclick="cambiarEstadoProducto(<?=$value["idproducto"]?>,0)">Inhabilitar</a>
                      <?php }else{ ?>
                      <a href="#" onclick="cambiarEstadoProducto(<?=$value["idproducto"]?>,1)">Activar</a>
                      <?php } ?>
                    </td>
                  </tr>

                <?php endforeach; ?>
  
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </div>