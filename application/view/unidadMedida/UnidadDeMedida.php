 <div class="row">
  <div class="col-md-2"></div>
            <div class="col-md-8">
              <div class="widget">
                <div class="widget-heading">
                  <h3 class="widget-title">UNIDAD DE MEDIDA</h3>
                </div>
                <div class="widget-body">
                  <form method="POST" action="<?php echo URL; ?>UnidadMedida/registrar" class="form-horizontal">
                   
                    <div class="form-group">
                      <label for="textInput">Codigo</label>
                      <input  type="number" required class="form-control" name="codigo" >
                    </div>
                     <div class="form-group">
                      <label for="textInput">Nombre</label>
                      <input  type="text" required class="form-control" name="nombre">
                    </div>
                    <div class="form-group">
                    <label for="textInput">Estado</label>
                       <select class="form-control required" name="estado">
                      <option value="0"></option>
                      <option value="1">ACTIVO</option>
                      <option value="0">INACTIVO</option>
                    </select>
                    </div>
                    
               <div class="form-actions">
                  <div>
                  <center>
                    <input class="btn btn-primary" type="submit" name="submit_guardar" value="REGISTRAR"/>  
                    </center>
                  </div>
                </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2"></div>
            <div class="col-lg-8">
          <div class="widget">
            <div class="widget-header"> <i class="icon-hand-up"></i>
              <h3>UNIDADES DE MEDIDA</h3>
            </div>
            <div class="widget-content">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>CODIGO</th>
                    <th>NOMBRE</th>
                    <th>ESTADO</th>
                    <th>OPCIONES</th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($datos as $value): ?>
                  <tr>
                    <td><?=$value["idUnidadMedida"]?></td>
                    <td><?=$value["nombre"]?></td>
                    <td><?=$value["estado"]==1?"Activo":"Inactivo"?></td>
                    <td>
                      
                      <a  href="<?php echo URL; ?>UnidadMedida/edit/<?=$value["idUnidadMedida"] ?>">editar</a>

                      <?php if ($value["estado"]==1){ ?>
                      <input value="Inhabilitar" type="button" class="btn btn-primary"  onclick="cambiarEstado(<?=$value["idUnidadMedida"] ?>,0)"/>
                      <?php }else{ ?>
                      <input value="Activar" type="button" class="btn btn-primary" onclick="cambiarEstado(<?=$value["idUnidadMedida"] ?>,1)"/>
                      <?php } ?>
                    </td>
                  </tr>

                <?php endforeach; ?>
  
                </tbody>
              </table>
            </div>
          </div>
        </div>
          </div>