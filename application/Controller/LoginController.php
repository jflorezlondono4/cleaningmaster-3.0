<?php  

namespace Mini\Controller;
use Mini\Model\mdlLogin;

class LoginController 
{
	
	

    private $mdlLogin = null;

    function __construct(){

       $this->mdlLogin = new mdlLogin();

    }

    public function index()
    {

        require APP . 'view/login/index.php';


    }


    public function logueo()
    {
      if (isset($_POST["login"])) {

         //header("location: ".URL."Home/index");

          $this->mdlLogin->__SET("Correo",$_POST["correo"]);
          $resultado = $this->mdlLogin->logueo();

// password_encrypt
          if ($resultado != false) {
            $contrasenaE = md5($_POST['clave']);
            if ($resultado["Contrasena"] == $contrasenaE ) {

              if ($resultado["Rol_Codigo"] == 1 ) {

                session_start();
                $_SESSION["idUsuario"] = $resultado["Codigo"];
                $_SESSION["Correo"] = $resultado["Correo"];
                $_SESSION["Rol"] = $resultado["Rol_Codigo"];



                header("location: ".URL."ordenServicio");



              }else   if ($resultado["Rol_Codigo"] == 2) {
                  session_start();
                  $_SESSION["idUsuario"] = $resultado["Codigo"];
                  $_SESSION["Correo"] = $resultado["Correo"];
                  $_SESSION["Rol"] = $resultado["Rol_Codigo"];

                  header("location: ".URL."ordenServicio");
                }

            }else {


              header('location: ' . URL . 'Login');

            }

          }else {

          header('location: ' . URL . 'Login');

          }
      }

    }
    public function salir()
    {


        //session_start();
        session_start();
        session_destroy();
        unset($_SESSION["idUsuario"]);

        unset($_SESSION["correo"]);
        unset($_SESSION["Rol"]);


        header('location: ' . URL . 'Login/index');


    }

}