<?php 
namespace Mini\Controller;
use Mini\Model\mdlServicio;

class ServicioController
{

	function __construct(){
        $this ->mdlServicio =  new mdlServicio(); 

    }
	public function index()
    {

    	$datos= $this->mdlServicio->listar();
        require APP . 'view/_templates/header.php';
        require APP . 'view/servicio/Servicio.php';
        require APP . 'view/_templates/footer.php';
    }

    public function registrar(){


        $this->mdlServicio->__SET("IdServicio", $_POST['codigo']);
        $this->mdlServicio->__SET("Nombre", $_POST['nombre']);
        $this->mdlServicio->__SET("Costo", $_POST['costo']);
        $this->mdlServicio->__SET("Estado", $_POST['estado']);
        $this->mdlServicio->__SET("Descripcion", $_POST['description']);
        $e = $this->mdlServicio->registrarServicio();
        header("location:".URL.'servicio/Servicio');
    }

    public function modificar(){
        $this ->mdlServicio->__SET("IdServicio",$_POST['codigo']);
        $this ->mdlServicio->__SET("Nombre",$_POST['nombre']);
        $this ->mdlServicio->__SET("Costo",$_POST['costo']);
        $this ->mdlServicio->__SET("Descripcion",$_POST['description']);
        $e = $this ->mdlServicio->modificarServicio();
        header("location:".URL.'servicio/Servicio');

        }

         public function edit($codi){

         $this ->mdlServicio->__SET("IdServicio",$codi);
         $datos= $this ->mdlServicio->consultarServicio();

        require APP . 'view/_templates/header.php';
        require APP . 'view/servicio/edit.php';
        require APP . 'view/_templates/footer.php';

        }

        public function cambiarEstado(){
        $this ->mdlServicio->__SET("IdServicio",$_POST["codigo"]);
        $this ->mdlServicio->__SET("Estado",$_POST["estado"]);
        $datos= $this ->mdlServicio->cambiarEstado();
        if ($datos) {
           echo json_encode(["b"=>1]);
        }else{
            echo json_encode(["b"=>0]);
        }
        // header("location:".URL."UnidadMedida/unidadMedida");
    }
}