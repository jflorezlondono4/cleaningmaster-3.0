<?php  
namespace Mini\Controller;
use Mini\Model\mdlProducto;
use Mini\Model\mdlKit;
use Mini\Model\mdlServicio;
use Mini\Model\mdlTipoVehiculo;



class KitController 
{

	function __construct(){
    $this->mdlProducto= new mdlProducto();
    $this->mdlkit= new mdlKit();
    $this->mdlServicio= new mdlServicio();
		$this->mdlTVehiculo= new mdlTipoVehiculo();
	}	

 public	function index() {
 		  $Producto=$this->mdlProducto->listar();
 		  $Kitt=$this->mdlkit->listarKit();
      $Servicio=$this->mdlServicio->listar();
      $Vehiculo=$this->mdlTVehiculo->listar();
 	    require APP . 'view/_templates/header.php';
      require APP . 'view/kit/Kit.php';
      require APP . 'view/_templates/footer.php';
   }

   public function botones(){
    if (isset($_POST["submit_guardar"])) {
        session_start();
        //session_destroy();
        unset($_SESSION["Codi"]);
        header("location:".URL."Kit/index");
    }elseif (isset($_POST["submit_cancelar"])) {
      session_start();
      $codi = $_SESSION["Codi"];
      $this->mdlkit->__SET("Codigo",$codi);
      $this->mdlkit->eliminarRegistro();
        //session_destroy();
        unset($_SESSION["Codi"]);
        header("location:".URL."Kit/index");
    }

   }

   public function registrarKit(){

    session_start();
    $_SESSION["Codi"] = $_POST["codigo"];


    $this->mdlkit->__SET("Codigo",$_POST["codigo"]);
    $this->mdlkit->__SET("Nombre",$_POST["nombre"]);
    $this->mdlkit->__SET("TVehiculo",$_POST["vehiculo"]);
    $this->mdlkit->__SET("Servicio",$_POST["servicio"]);
    $this->mdlkit->__SET("Estado",$_POST["estado"]);
    $this->mdlkit->registrarKit();
    

   }
   public function registrarDetalle(){
    session_start();
    $codi = $_SESSION["Codi"];
    $this->mdlkit->__SET("Codigo",$codi);
    $this->mdlkit->__SET("Cantidad",$_POST["cantida"]);
    $this->mdlkit->__SET("Producto",$_POST["product"]);
    $this->mdlkit->registrarDetalle();
    $deta=$this->mdlkit->consultarDetalle();
    echo json_encode($deta);
   }
}
