<?php  
namespace Mini\Controller;
use Mini\Model\mdlMovimiento;
use Mini\Model\mdlProducto;
/**
* 
*/
class entradaYsalidaController
{
	function __construct(){
		$this->mdlProducto = new mdlProducto();
		$this->mdlMovimiento = new mdlMovimiento();
	}
	
	public function index()
    {
    	$productos=$this->mdlProducto->listar();
    	$this ->mdlMovimiento->__SET("Tipo",1);
    	$Entrada=$this ->mdlMovimiento->listarMovimiento();
    	$this ->mdlMovimiento->__SET("Tipo",2);
    	$Salida=$this ->mdlMovimiento->listarMovimiento();

        require APP . 'view/_templates/header.php';
        require APP . 'view/entradaYsalida/Movimiento.php';
        require APP . 'view/_templates/footer.php';
    }

    public function registrarEntrada(){

        $this->mdlProducto->__SET("idProducto",$_POST['producto']);
        $productos=$this->mdlProducto->consultarProducto();
        $cantidadTotal=$productos['cantidad']+$_POST['cantidad'];
        $this->mdlProducto->__SET("Cantidad",$cantidadTotal);
        $a=$this->mdlProducto->modificarCantidad();

    	$this->mdlMovimiento->__SET("Codigo",$_POST['codigo']);
    	$this->mdlMovimiento->__SET("Cantidad",$_POST['cantidad']);
    	$this->mdlMovimiento->__SET("Producto",$_POST['producto']);
    	$this->mdlMovimiento->__SET("Descripcion",$_POST['descripcion']);
        $this->mdlMovimiento->__SET("Tipo",1);
    	$a=$this->mdlMovimiento->registrarMovimiento();
    	header("location:".URL."entradaYsalida/index");
    }

    public function registrarSalida(){

        $this->mdlProducto->__SET("idProducto",$_POST['producto']);
        $productos=$this->mdlProducto->consultarProducto();
        $cantidadTotal=$productos['cantidad']-$_POST['cantidad'];
        $this->mdlProducto->__SET("Cantidad",$cantidadTotal);
        $a=$this->mdlProducto->modificarCantidad();


    	$this->mdlMovimiento->__SET("Codigo",$_POST['codigo']);
    	$this->mdlMovimiento->__SET("Cantidad",$_POST['cantidad']);
    	$this->mdlMovimiento->__SET("Producto",$_POST['producto']);
    	$this->mdlMovimiento->__SET("Descripcion",$_POST['descripcion']);
    	$this->mdlMovimiento->__SET("Tipo",2);
    	$a=$this->mdlMovimiento->registrarMovimiento();
    	header("location:".URL."entradaYsalida/index");
    }
}