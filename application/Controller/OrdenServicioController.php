<?php  

namespace Mini\Controller;

use Mini\Model\mdlEmpleado;
use Mini\Model\mdlCliente;
use Mini\Model\mdlServicio;
use Mini\Model\mdlTipoVehiculo;
use Mini\Model\mdlOrdenDeServicio;

class OrdenServicioController
{
	function __construct(){


        $this ->mdlCliente =  new mdlCliente();
         $this ->mdlEmpleado =  new mdlEmpleado();
         $this ->mdlServicio =  new mdlServicio();
         $this->mdlTipoVehiculo = new mdlTipoVehiculo();
         $this->mdlOrdenDeServicio = new mdlOrdenDeServicio();
    }
   
	public function index()
    {
    	$cliente= $this->mdlCliente->listarClienteActivo();
        $empleado=$this->mdlEmpleado->listarEmpleadoActivo();
        $servicios=$this->mdlServicio->listarServicioActivo();
        $Vehiculo =$this->mdlTipoVehiculo->listarTipoVehiculoActivo();
        require APP . 'view/_templates/header.php';
        require APP . 'view/ordenServicio/OrdenDeServicio.php';
        require APP . 'view/_templates/footer.php';
    }

    public function botones(){
    if (isset($_POST["submit_guardar"])) {
        session_start();
        //session_destroy();
        unset($_SESSION["Codi"]);
        header("location:".URL."OrdenDeServicio/index");
    }elseif (isset($_POST["submit_cancelar"])) {
      session_start();
      $codi = $_SESSION["Codi"];
      $this->mdlOrdenDeServicio->__SET("Codigo",$codi);
      $this->mdlOrdenDeServicio->eliminarRegistro();
        //session_destroy();
        unset($_SESSION["Codi"]);
        header("location:".URL."OrdenDeServicio/index");
    }

   }

   public function registrarDetalleOrden(){
    session_start();
    $codi = $_SESSION["Codi"];
    $this->mdlOrdenDeServicio->__SET("Codigo",$codi);
    $this->mdlOrdenDeServicio->__SET("Servicios",$_POST["servicios"]);
    $this->mdlOrdenDeServicio->registrarDetalleOrden();
    $deta=$this->mdlOrdenDeServicio->consultarDetalle();
    echo json_encode($deta);
   }


    public function registrarOrdenServicio(){

    $this->mdlOrdenDeServicio->__SET("Fecha",$_POST["fecha"]);
    $this->mdlOrdenDeServicio->__SET("Estado",$_POST["estado"]);
    $this->mdlOrdenDeServicio->__SET("Empleado",$_POST["empleado"]);
    $this->mdlOrdenDeServicio->__SET("Marca",$_POST["marca"]);
    $this->mdlOrdenDeServicio->__SET("Descripcion",$_POST["descripcion"]);
    $this->mdlOrdenDeServicio->__SET("Placa",$_POST["placa"]);
    $this->mdlOrdenDeServicio->__SET("Cliente",$_POST["cliente"]);
    $this->mdlOrdenDeServicio->__SET("Tvehiculo",$_POST["tvehiculo"]);
    // var_dump($_POST["fecha"],$_POST["estado"], $_POST["empleado"], $_POST["marca"],$_POST["descripcion",$_POST["placa"],$_POST["cliente"], $_POST["tvehiculo"],$_POST["tvehiculo"]);
    // exit();
    
    $this->mdlOrdenDeServicio->registrarOrdenServicio();
    }

}