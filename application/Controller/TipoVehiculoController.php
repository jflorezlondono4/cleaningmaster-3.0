<?php  

namespace Mini\Controller;
use Mini\Model\mdlTipoVehiculo;
/**
* 
*/

class TipoVehiculoController 
{
	function __construct(){
        $this ->mdlTipoVehiculo =  new mdlTipoVehiculo(); 

	}
	

	public function index()
    {
    	//Obtiene tipovehiculo
    	$datos= $this->mdlTipoVehiculo->listar();

    	//Carga vistas
        require APP . 'view/_templates/header.php';
        require APP . 'view/tipoVehiculo/TipoDeVehiculo.php';
        require APP . 'view/_templates/footer.php';
}

        // public function guardarTipoVehiculoName(){

        // 	if (isset($_POST["guardar"])) {
        // 		$this->mdlTipoVehiculo->registrarTipoVehiculo($_POST["idtipo_vehiculo"], $_POST["nombre"], $_POST["estado"], $_POST["observaciones"]);

        // 		header('location'. URL . 'tipoVehiculo/tipoVehiculo');
        // 	}
        // }

        public function registrar(){
        
        $this ->mdlTipoVehiculo->__SET("IdTipoVehiculo",$_POST['codigo']);
        $this ->mdlTipoVehiculo->__SET("Nombre",$_POST['nombre']);
        $this ->mdlTipoVehiculo->__SET("Estado",$_POST['estado']);
        $this ->mdlTipoVehiculo->__SET("Descripcion",$_POST['description']);
        $e = $this ->mdlTipoVehiculo->registrarTVehiculo();
        header("location:".URL.'tipoVehiculo/tipoVehiculo');
        }

        public function modificar(){
        $this ->mdlTipoVehiculo->__SET("IdTipoVehiculo",$_POST['codigo']);
        $this ->mdlTipoVehiculo->__SET("Nombre",$_POST['nombre']);
        $this ->mdlTipoVehiculo->__SET("Descripcion",$_POST['description']);
        $e = $this ->mdlTipoVehiculo->modificarTVehiculo();
        header("location:".URL.'tipoVehiculo/tipoVehiculo');

        }

        public function edit($codi){

         $this ->mdlTipoVehiculo->__SET("IdTipoVehiculo",$codi);
         $datos= $this ->mdlTipoVehiculo->consultarTVehiculo();

        require APP . 'view/_templates/header.php';
        require APP . 'view/tipovehiculo/edit.php';
        require APP . 'view/_templates/footer.php';

        }

        public function cambiarEstado(){
        $this ->mdlTipoVehiculo->__SET("IdTipoVehiculo",$_POST["codigo"]);
        $this ->mdlTipoVehiculo->__SET("Estado",$_POST["estado"]);
        $datos= $this ->mdlTipoVehiculo->cambiarEstado();
        if ($datos) {
           echo json_encode(["b"=>1]);
        }else{
            echo json_encode(["b"=>0]);
        }
        // header("location:".URL."UnidadMedida/unidadMedida");
    }

        // public function dameTipoVehiculoName($idtipo_vehiculo){

        // 	if (isset($idtipo_vehiculo)) {
        // 		$this->mdlTipoVehiculo->registrarTipoVehiculo($_POST["idtipo_vehiculo"], $_POST["nombre"], $_POST["estado"], $_POST["observaciones"]);

        // 		header('location '. URL . 'tipoVehiculo/tipoVehiculo');
        // 	}
        // }
        // public function modificarTipoVehiculoName($idtipo_vehiculo){

        // 	if ( isset($idtipo_vehiculo)){
        // 		$this->mdlTipoVehiculo->registrarTipoVehiculo($_POST["idtipo_vehiculo"], $_POST["nombre"], $_POST["estado"], $_POST["observaciones"]);

        // 		header('location '. URL . 'tipoVehiculo/tipoVehiculo');
        // 	}
        // }

    
}