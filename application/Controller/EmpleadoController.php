<?php  
namespace Mini\Controller;

use Mini\Model\mdlEmpleado;

class EmpleadoController
{
	function __construct(){
        $this ->mdlEmpleado =  new mdlEmpleado();
    }
	public function index()
    {
        $datos= $this->mdlEmpleado->listarEmpleado();
        $persona=$this->mdlEmpleado->listarTipoPersona();
        
        require APP . 'view/_templates/header.php';
        require APP . 'view/empleado/Empleado.php';
        require APP . 'view/_templates/footer.php';
    }
     public function guardarEmpleado(){

       
        	$this ->mdlEmpleado->__SET("tipoDocumento",$_POST['dd1']);
            $this ->mdlEmpleado->__SET("documento",$_POST['cedula']);
            $this ->mdlEmpleado->__SET("nombre",$_POST['Nombres']);
            $this ->mdlEmpleado->__SET("Tipo_Persona_idTipo_Persona",$_POST['dd2']);
            $this ->mdlEmpleado->__SET("contacto",$_POST['contacto']);
            $this ->mdlEmpleado->__SET("direccion",$_POST['direccion']);
            $this ->mdlEmpleado->__SET("estado",$_POST['dd3']);
            $e = $this ->mdlEmpleado->registrarEmpleado();
            header("location:".URL."empleado/Empleado");

        
    }
    public function alterarEmpleado(){

        if (isset($_POST["submit_modificar"])) {
            $this ->mdlEmpleado->__SET("tipoDocumento",$_POST['dd1']);
            $this ->mdlEmpleado->__SET("documento",$_POST['cedula']);
            $this ->mdlEmpleado->__SET("nombre",$_POST['Nombres']);
            $this ->mdlEmpleado->__SET("Tipo_Persona_idTipo_Persona",$_POST['dd2']);
            $this ->mdlEmpleado->__SET("contacto",$_POST['contacto']);
            $this ->mdlEmpleado->__SET("direccion",$_POST['direccion']);
            $this ->mdlEmpleado->__SET("estado",$_POST['dd3']);
            $e = $this ->mdlEmpleado->modificarEmpleado();
            header("location:".URL."empleado/Empleado");
        }
    }
    
    public function cambiarEstado(){
        $this ->mdlEmpleado->__SET("documento",$_POST["cedula"]);
        $this ->mdlEmpleado->__SET("Estado",$_POST["estado"]);
        $datos= $this ->mdlEmpleado->cambiarEstadoEmpleado();
        if ($datos) {
           echo json_encode(["b"=>1]);
        }else{
            echo json_encode(["b"=>0]);
        }
        // header("location:".URL."UnidadMedida/unidadMedida");
    }
    public function edit($codi){

         $this ->mdlEmpleado->__SET("documento",$codi);
         $datos= $this ->mdlEmpleado->consultarEmpleado();

        require APP . 'view/_templates/header.php';
        require APP . 'view/Empleado/edit.php';
        require APP . 'view/_templates/footer.php';

    }
}