<?php  

namespace Mini\Controller;
use Mini\Model\mdlUnidadMedida;
use Mini\Model\mdlProducto;
/**
* 
*/
class ProductoController
{
	function __construct(){
        $this ->mdlUnidadMedida =  new mdlUnidadMedida();
        $this ->mdlProducto =  new mdlProducto();
    }
	
    public function index()
    {
    	$Medida= $this ->mdlUnidadMedida->listar();
    	$Datos= $this ->mdlProducto->listar();
        require APP . 'view/_templates/header.php';
        require APP . 'view/producto/Producto.php';
        require APP . 'view/_templates/footer.php';
    }

    public function edit($codi){
        $this ->mdlProducto->__SET("idProducto",$codi);
        $Cons= $this ->mdlProducto->consultarProducto();
        $Medida= $this ->mdlUnidadMedida->listar();
        require APP . 'view/_templates/header.php';
        require APP . 'view/producto/edit.php';
        require APP . 'view/_templates/footer.php';

    }

    public function registrar(){

    	$this ->mdlProducto->__SET("idProducto",$_POST['codigo']);
    	$this ->mdlProducto->__SET("Nombre",$_POST['nombre']);
    	$this ->mdlProducto->__SET("PUnitario",$_POST['precio']);
    	$this ->mdlProducto->__SET("Cantidad",$_POST['cantidad']);
    	$this ->mdlProducto->__SET("UMedida",$_POST['medida']);
    	$this ->mdlProducto->__SET("Estado",$_POST['estado']);
    	$e = $this ->mdlProducto->registrarp();
    	header("location:".URL."Producto/producto");
    }
    public function modificar(){
        $this ->mdlProducto->__SET("idProducto",$_POST['codigo']);
        $this ->mdlProducto->__SET("Nombre",$_POST['nombre']);
        $this ->mdlProducto->__SET("PUnitario",$_POST['precio']);
        $this ->mdlProducto->__SET("Cantidad",$_POST['cantidad']);
        $this ->mdlProducto->__SET("UMedida",$_POST['medida']);
        $e = $this ->mdlProducto->modificarProducto();
        header("location:".URL."Producto/producto");
    }

    public function cambiarEstado(){

        $this ->mdlProducto->__SET("idProducto",$_POST["codigo"]);
        $this ->mdlProducto->__SET("Estado",$_POST["estado"]);
        $datos= $this ->mdlProducto->cambiarEstado();
        if ($datos) {
           echo json_encode(["b"=>1]);
        }else{
            echo json_encode(["b"=>0]);
        }
    }
}