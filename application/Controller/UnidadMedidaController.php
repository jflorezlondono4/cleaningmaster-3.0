<?php 
namespace Mini\Controller;
use Mini\Model\mdlUnidadMedida;

class UnidadMedidaController 
{
    function __construct(){
        $this ->mdlUnidadMedida =  new mdlUnidadMedida();
    }

	public function index()
    {
      
      $datos= $this ->mdlUnidadMedida->listar();

        require APP . 'view/_templates/header.php';
        require APP . 'view/unidadMedida/UnidadDeMedida.php';
        require APP . 'view/_templates/footer.php';
    }

    
    // public function botones(){

    //     if (isset($_POST["submit_guardar"])) {
    //         $this ->mdlUnidadMedida->__SET("idUnidadMedida",$_POST['codigo']);
    //         $this ->mdlUnidadMedida->__SET("Nombre",$_POST['nombre']);
    //         $this ->mdlUnidadMedida->__SET("Estado",$_POST['estado']);
    //         $e = $this ->mdlUnidadMedida->registrar();
    //         header("location:".URL."UnidadMedida/unidadMedida");

    //     }
    //     if (isset($_POST["submit_modificar"])) {
    //         $this ->mdlUnidadMedida->__SET("idUnidadMedida",$_POST['codigo']);
    //         $this ->mdlUnidadMedida->__SET("Nombre",$_POST['nombre']);
    //         $e = $this ->mdlUnidadMedida->modificar();
    //         header("location:".URL."UnidadMedida/unidadMedida");
    //     }
    // }
    public function cambiarEstado(){
        $this ->mdlUnidadMedida->__SET("idUnidadMedida",$_POST["codigo"]);
        $this ->mdlUnidadMedida->__SET("Estado",$_POST["estado"]);
        $datos= $this ->mdlUnidadMedida->cambiarEstado();
        if ($datos) {
           echo json_encode(["b"=>1]);
        }else{
            echo json_encode(["b"=>0]);
        }
        // header("location:".URL."UnidadMedida/unidadMedida");
    }
    public function edit($codi){

         $this ->mdlUnidadMedida->__SET("idUnidadMedida",$codi);
         $datos= $this ->mdlUnidadMedida->consultar();

        require APP . 'view/_templates/header.php';
        require APP . 'view/unidadMedida/edit.php';
        require APP . 'view/_templates/footer.php';

    }


	public function registrar(){
        
    $this ->mdlUnidadMedida->__SET("idUnidadMedida",$_POST['codigo']);
    $this ->mdlUnidadMedida->__SET("Nombre",$_POST['nombre']);
    $this ->mdlUnidadMedida->__SET("Estado",$_POST['estado']);
    $e = $this ->mdlUnidadMedida->registrar();
    header("location:".URL."UnidadMedida/unidadMedida");
	}

    public function modificar(){
    $this ->mdlUnidadMedida->__SET("idUnidadMedida",$_POST['codigo']);
    $this ->mdlUnidadMedida->__SET("Nombre",$_POST['nombre']);
    $e = $this ->mdlUnidadMedida->modificar();
    header("location:".URL."UnidadMedida/unidadMedida");

    }

    
}